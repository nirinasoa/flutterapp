import 'package:flutter/material.dart';

void main() => runApp(MaterialApp(
    home:Home()
  ));

class Home extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return  Scaffold(
      appBar: AppBar(
        title: Text('First App'),
        centerTitle: true,
        backgroundColor: Colors.redAccent,
      ),
      body:Row(
        children: <Widget>[
          Expanded(
            child: Container(
              padding: EdgeInsets.all(30),
              color:Colors.cyan,
              child: Text('1'),

            ),
          ),
          Expanded(
            child: Container(
              padding: EdgeInsets.all(30),
              color:Colors.green,
              child: Text('2'),
            ),
          ),
          Expanded(
            child: Container(
              padding: EdgeInsets.all(30),
              color:Colors.yellowAccent,
              child: Text('3'),
            ),
          ),
        ],
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {},
        child: Text('Click '),
        backgroundColor: Colors.redAccent,
      ),
    );
  }
}

  



